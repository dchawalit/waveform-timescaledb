package com.etiometry.waveform;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.dellroad.hl7.HL7Field;
import org.dellroad.hl7.HL7Message;

import com.etiometry.waveform.model.Patient;
import com.etiometry.waveform.model.WaveformObservation;
import com.etiometry.waveform.model.WaveformStringObservation;

public class WaveformReader {
    private final String SAMPLE_RATE_ID = "MDC_ATTR_SAMPLE_RATE";

    private final Map<String, Short> labelToWaveformId = new HashMap<>();

    public WaveformReader() {
        labelToWaveformId.put("MDC_ECG_ELEC_POTL_I", (short) 1);
        labelToWaveformId.put("MDC_ECG_ELEC_POTL_II", (short) 2);
        labelToWaveformId.put("MDC_ECG_ELEC_POTL_V2", (short) 3);
        labelToWaveformId.put("MDC_ECG_ELEC_POTL_III", (short) 4);
        labelToWaveformId.put("MDC_ECG_ELEC_POTL_AVR", (short) 5);
        labelToWaveformId.put("MDC_ECG_ELEC_POTL_AVL", (short) 6);
        labelToWaveformId.put("MDC_ECG_ELEC_POTL_AVF", (short) 7);
        labelToWaveformId.put("MDC_IMPED_TTHOR", (short) 8);
    }

    public List<WaveformObservation> read(Patient patient, HL7Message msg) {
        List<WaveformObservation> obsList = new ArrayList<>();

        AtomicLong waveStartTime = new AtomicLong();
        AtomicLong waveEndTime = new AtomicLong();

        msg.getSegments().stream().forEach(segment -> {
            switch (segment.getName()) {
            case "OBR":
                HL7Field obr4 = segment.getField(4);
                String obrServiceId = obr4.get(0, 1, 0);

                if (obrServiceId.equals("MDC_OBS_WAVE_CTS")) {
                    String start = segment.getField(7).toString();
                    String end = segment.getField(8).toString();

                    waveStartTime.set(getTimestamp(start));
                    waveEndTime.set(getTimestamp(end));
                }

                break;

            case "OBX":
                HL7Field type = segment.getField(2);
                HL7Field labelField = segment.getField(3);
                HL7Field valueField = segment.getField(5);
                // HL7Field unitsField = segment.getField(6);

                String label = labelField.get(0, 1, 0);

                if (type.toString().equals("NA")) {

                    String[][][] valueArray = valueField.getValue();

                    /**
                     * values as defined in this structure as:
                     * 
                     * <pre>
                     * String value1 = valueArray[0][0][0]; 
                     * String value2 = valueArray[0][1][0]; 
                     * ...
                     * </pre>
                     */
                    List<Double> values = Arrays.asList(valueArray[0]).stream()
                        .map(element -> Double.parseDouble(element[0]))
                        .collect(Collectors.toList());

                    long period = (waveEndTime.get() - waveStartTime.get()) / values.size();

                    for (int i = 0; i < values.size(); i++) {
                        WaveformObservation o = new WaveformObservation();

                        o.setId(UUID.randomUUID().toString());
                        o.setFkpatient(patient.getId());
                        o.setObsTimestamp(waveStartTime.get() + (i * period));
                        o.setValue(values.get(i));
                        o.setWaveformId(labelToWaveformId.get(label));

                        obsList.add(o);
                    }
                }

                break;
            }
        });

        return obsList;
    }

    public List<WaveformStringObservation> readWaveformAsStringObservations(Patient patient, HL7Message msg) {
        List<WaveformStringObservation> obsList = new ArrayList<>();

        AtomicLong waveStartTime = new AtomicLong();
        AtomicLong waveEndTime = new AtomicLong();

        msg.getSegments().stream().forEach(segment -> {
            switch (segment.getName()) {
            case "OBR":
                HL7Field obr4 = segment.getField(4);
                String obrServiceId = obr4.get(0, 1, 0);

                if (obrServiceId.equals("MDC_OBS_WAVE_CTS")) {
                    String start = segment.getField(7).toString();
                    String end = segment.getField(8).toString();

                    waveStartTime.set(getTimestamp(start));
                    waveEndTime.set(getTimestamp(end));
                }

                break;

            case "OBX":
                HL7Field type = segment.getField(2);
                HL7Field labelField = segment.getField(3);
                HL7Field valueField = segment.getField(5);
                // HL7Field unitsField = segment.getField(6);

                String label = labelField.get(0, 1, 0);

                if (type.toString().equals("NA")) {
                    WaveformStringObservation o = new WaveformStringObservation();

                    o.setId(UUID.randomUUID().toString());
                    o.setFkpatient(patient.getId());
                    o.setStartTime(waveStartTime.get());
                    o.setEndTime(waveEndTime.get());
                    o.setValues(valueField.toString());
                    o.setWaveformId(labelToWaveformId.get(label));

                    obsList.add(o);
                } else if (label.equals(SAMPLE_RATE_ID)) {
                    obsList.get(obsList.size() - 1).setSampleRate(Integer.parseInt(valueField.toString()));
                }

                break;
            }
        });

        return obsList;
    }

    private long getTimestamp(String time) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss.SSSSZ");
        LocalDateTime ldt = LocalDateTime.parse(time, dtf);

        return ldt.toInstant(ZoneOffset.UTC).toEpochMilli();
    }

}
