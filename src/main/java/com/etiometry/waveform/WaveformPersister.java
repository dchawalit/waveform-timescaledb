package com.etiometry.waveform;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.hibernate.Session;

import com.etiometry.waveform.db.DBConnector;
import com.etiometry.waveform.model.WaveformObservation;
import com.etiometry.waveform.model.WaveformStringObservation;

public class WaveformPersister {
    public void persistByParallelCopy(List<WaveformObservation> observations) throws Exception {
        long t0 = System.currentTimeMillis();
        StringBuffer buffer = new StringBuffer();

        // use StringBuffer and for-loop for speed
        for (int i = 0; i < observations.size(); i++) {
            WaveformObservation o = observations.get(i);
            buffer.append(o.getId() + "," + Long.toString(o.getObsTimestamp()) + "," + o.getWaveformId() + "," + o
                .getValue() + "," + o.getFkpatient());
            buffer.append(System.lineSeparator());
        }

        long t1 = System.currentTimeMillis();
        // System.out.println("mapping all observations to string format: " + (t1 - t0) + "ms");

        long t2 = System.currentTimeMillis();
        runCommand("timescaledb-parallel-copy --db-name waveform --table waveform_observation --workers 4", buffer
            .toString());
        long t3 = System.currentTimeMillis();

        // System.out.println("copying observations to database: " + (t3 - t2) + "ms");
    }

    public void persistByBatchInsert(List<WaveformObservation> observations, int batchSize) {
        EntityManager em = DBConnector.getEntityManager();
        // System.out.println("init hibernate batch size: " + em.getProperties().get("hibernate.jdbc.batch_size"));

        // em.setProperty("hibernate.jdbc.batch_size", batchSize);
        // System.out.println("hibernate batch size2: " + em.getProperties().get("hibernate.jdbc.batch_size"));

        em.unwrap(Session.class).setJdbcBatchSize(batchSize);
        // System.out.println("batch size2 : " + em.getProperties().get("hibernate.jdbc.batch_size"));

        EntityTransaction tx = em.getTransaction();
        tx.begin();

        for (int i = 0; i < observations.size(); i++) {
            if (i > 0 && i % batchSize == 0) {
                em.flush();
                em.clear();
            }

            em.persist(observations.get(i));
        }

        tx.commit();
        em.close();
    }

    private void runCommand(String command, String obsList) {
        ProcessBuilder pb = new ProcessBuilder("bash", "-c", command);
        List<String> output = new ArrayList<>();

        BufferedReader br = null;

        try {
            Process p = pb.start();

            OutputStream stdin = p.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

            writer.write(obsList);
            writer.flush();
            writer.close();

            br = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = null;
            while ((line = br.readLine()) != null) {
                output.add(line);
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        output.stream().forEach(System.out::println);
    }

    public void persistStringObservation(List<WaveformStringObservation> stringObservations) {
        try {
            EntityManager em = DBConnector.getEntityManager();

            EntityTransaction tx = em.getTransaction();
            tx.begin();

            for (int i = 0; i < stringObservations.size(); i++) {
                em.persist(stringObservations.get(i));
            }

            tx.commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
