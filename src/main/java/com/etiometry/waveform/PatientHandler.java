package com.etiometry.waveform;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.etiometry.waveform.db.DBConnector;
import com.etiometry.waveform.model.Patient;
import com.etiometry.waveform.model.Patient_;

public class PatientHandler {

    public Patient save(String mrn, String lastName, String firstName) {
        Patient dbPatient = find(mrn);

        if (dbPatient == null) {
            // System.out.println("Patient [" + mrn + "] does not exist");

            Patient p = new Patient();

            p.setMrn(mrn);
            p.setLastName(lastName);
            p.setFirstName(firstName);

            long t2 = System.currentTimeMillis();

            EntityManager em = DBConnector.getEntityManager();

            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(p);
            tx.commit();
            em.close();

            long t3 = System.currentTimeMillis();
            // System.out.println("Persist time: " + (t3 - t2) + "ms");

            return find(mrn);
        } else {
            // System.out.println("Patient [" + mrn + "] exists");
            return dbPatient;
        }

    }

    private Patient find(String mrn) {
        long t0 = System.currentTimeMillis();

        EntityManager em = DBConnector.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Patient> criteria = builder.createQuery(Patient.class);
        Root<Patient> root = criteria.from(Patient.class);
        criteria.select(root).where(builder.equal(root.get(Patient_.mrn), mrn));

        List<Patient> patients = em.createQuery(criteria).getResultList();
        tx.commit();
        em.close();

        long t1 = System.currentTimeMillis();
        // System.out.println("Patient search: " + (t1 - t0) + "ms");

        return patients.isEmpty() ? null : patients.get(0);
    }
}
