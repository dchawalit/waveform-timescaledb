package com.etiometry.waveform.db;

import static javax.persistence.Persistence.createEntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class DBConnector {
    private static final String PERSISTENCE_UNIT = "com.etiometry.waveform.jpa";
    private static EntityManagerFactory FACTORY;
    private static boolean INITIALIZED = false;

    public static void init() {
        if (!INITIALIZED) {
            INITIALIZED = true;

            long t0 = System.currentTimeMillis();
            FACTORY = createEntityManagerFactory(PERSISTENCE_UNIT);
            long t1 = System.currentTimeMillis();

            System.out.println("*** Initialized entity manager factory: " + (t1 - t0) + "ms");
        }
    }

    public static EntityManager getEntityManager() {
        if (!INITIALIZED) {
            init();
        }

        return FACTORY.createEntityManager();
    }

}
