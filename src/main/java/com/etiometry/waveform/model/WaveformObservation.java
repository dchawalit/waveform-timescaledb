package com.etiometry.waveform.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "waveform_observation")
public class WaveformObservation {
    @Id
    private String id;

    private Long obsTimestamp;
    private Short waveformId;
    private Double value;
    private Long fkpatient;

    public WaveformObservation() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getObsTimestamp() {
        return obsTimestamp;
    }

    public void setObsTimestamp(Long obsTimestamp) {
        this.obsTimestamp = obsTimestamp;
    }

    public Short getWaveformId() {
        return waveformId;
    }

    public void setWaveformId(Short waveformId) {
        this.waveformId = waveformId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getFkpatient() {
        return fkpatient;
    }

    public void setFkpatient(Long fkpatient) {
        this.fkpatient = fkpatient;
    }

}
