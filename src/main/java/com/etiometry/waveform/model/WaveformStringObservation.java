package com.etiometry.waveform.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "waveform_string_observation")
public class WaveformStringObservation {
    @Id
    private String id;

    private Long startTime;
    private Long endTime;
    private Short waveformId;
    private String values;
    private int sampleRate;
    private Long fkpatient;

    public WaveformStringObservation() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Short getWaveformId() {
        return waveformId;
    }

    public void setWaveformId(Short waveformId) {
        this.waveformId = waveformId;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
    }

    public Long getFkpatient() {
        return fkpatient;
    }

    public void setFkpatient(Long fkpatient) {
        this.fkpatient = fkpatient;
    }

}
