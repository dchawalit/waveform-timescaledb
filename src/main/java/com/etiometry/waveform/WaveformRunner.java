package com.etiometry.waveform;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;

import org.dellroad.hl7.HL7Message;
import org.dellroad.hl7.HL7Reader;
import org.dellroad.hl7.io.HL7FileReader;

import com.etiometry.waveform.db.DBConnector;

public class WaveformRunner {
    private final MessageHandler messageHandler;

    public WaveformRunner() {
        this.messageHandler = new MessageHandler();
    }

    public void run(String fileName, String persistType, String batchSize, String obsType) throws Exception {
        Path file = Paths.get(fileName);

        AtomicInteger count = new AtomicInteger(1);

        Path path = FileSystems.getDefault().getPath(fileName);

        if (path.toFile().isDirectory()) {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    System.out.println("reading " + file.getFileName());

                    readFile(file, count, persistType, batchSize, obsType);

                    return FileVisitResult.CONTINUE;
                }
            });
        } else {
            readFile(file, count, persistType, batchSize, obsType);
        }
    }

    private void readFile(Path file, AtomicInteger count, String persistType, String batchSize, String obsType)
        throws IOException {
        try (HL7Reader reader = new HL7FileReader(new BufferedInputStream(new FileInputStream(file
            .toFile())))) {
            while (true) {
                try {
                    HL7Message msg = reader.readMessage();

                    System.out.println("!!! reading message: " + count.get());
                    messageHandler.handle(msg, persistType, Integer.parseInt(batchSize), obsType);
                    System.out.println("!!! finished message: " + count.getAndAdd(1));
                } catch (Exception e) {
                    break;
                }
            }
        }

    }

    public static void main(String[] args) throws Exception {
        if (args.length != 4) {
            System.out.println("Does not match expected number of arguments");
            System.exit(1);
        }

        DBConnector.init();

        String file = args[0];
        String insertType = args[1];
        String batchInsert = args[2];
        String obsType = args[3];

        WaveformRunner runner = new WaveformRunner();

        long t0 = System.currentTimeMillis();
        // runner.run(file, insertType, batchInsert, obsType);
        long t1 = System.currentTimeMillis();

        System.out.println("total insert time: " + (t1 - t0) + "ms");

        // long t2 = System.currentTimeMillis();
        WaveformRetriever retriever = new WaveformRetriever();
        retriever.retrieve();
        // long t3 = System.currentTimeMillis();

        // System.out.println("total retrieve time: " + (t3 - t2) + "ms");
    }

}
