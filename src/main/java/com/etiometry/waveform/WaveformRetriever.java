package com.etiometry.waveform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.etiometry.waveform.db.DBConnector;
import com.etiometry.waveform.model.WaveformStringObservation;
import com.etiometry.waveform.model.WaveformStringObservation_;
import com.google.common.math.Stats;

public class WaveformRetriever {
    static final long beginning = 1617130793000L;
    static final long end = 1617217193000L;
    static final int numTimes = 1000;

    public void retrieve() {
        List<Long> fullTimes = new ArrayList<>();
        List<Long> fetchTimes = new ArrayList<>();
        List<Long> transformTimes = new ArrayList<>();

        Random random = new Random();
        int duration = 20;

        List<WaveformStringObservation> observations = new ArrayList<>();

        for (int i = 0; i < numTimes; i++) {
            long t0 = System.currentTimeMillis();

            EntityManager em = DBConnector.getEntityManager();
            EntityTransaction transaction = em.getTransaction();

            String patientId = Integer.toString(random.ints(1, 1, 4).findFirst().getAsInt());
            long startTime = random.longs(1, beginning, end).findFirst().getAsLong();
            long endTime = startTime + TimeUnit.MINUTES.toMillis(duration);

            transaction.begin();
            long t1 = System.currentTimeMillis();
            observations = fetch(em, startTime, endTime, patientId);
            long t2 = System.currentTimeMillis();
            transaction.commit();

            fetchTimes.add(t2 - t1);
            em.close();

            long t3 = System.currentTimeMillis();
            transform(observations);
            long t4 = System.currentTimeMillis();

            transformTimes.add(t4 - t3);

            fullTimes.add(t4 - t0);
        }

        Map<Short, List<TimeValuePair>> timeValuePairs = transform(observations);
        timeValuePairs.entrySet().stream()
            .forEach(entrySet -> {
                System.out.println("waveformId: " + entrySet.getKey() + ", numPairs: " + entrySet.getValue().size());

                Stats pairs = Stats.of(entrySet.getValue().stream()
                    .map(TimeValuePair::getTime)
                    .collect(Collectors.toList()));

                Function<Double, Long> toLong = d -> new BigDecimal(d).longValue();
                System.out.println("TVP time stats - count: " + pairs.count() + ", min = " + toLong.apply(pairs.min())
                    + ", max = " + toLong.apply(pairs.max()));
            });

        System.out.println("retrieval stats: " + Stats.of(fetchTimes).toString());
        System.out.println("transform stats: " + Stats.of(transformTimes).toString());
        System.out.println("full retrieval stats: " + Stats.of(fullTimes).toString());
    }

    private List<WaveformStringObservation> fetch(EntityManager em, long startTime, long endTime,
        String patientId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<WaveformStringObservation> cq = cb.createQuery(WaveformStringObservation.class);

        Root<WaveformStringObservation> root = cq.from(WaveformStringObservation.class);

        Predicate p1 = cb.ge(root.get(WaveformStringObservation_.START_TIME), startTime);
        Predicate p2 = cb.le(root.get(WaveformStringObservation_.START_TIME), endTime);
        Predicate p3 = cb.equal(root.get(WaveformStringObservation_.FKPATIENT), patientId);

        cq.select(root).where(p1, p2, p3);

        return em.createQuery(cq).getResultList();
    }

    private Map<Short, List<TimeValuePair>> transform(List<WaveformStringObservation> observations) {
        Map<Short, List<TimeValuePair>> timeValuePairs = observations.stream()
            .collect(Collectors.groupingBy(WaveformStringObservation::getWaveformId, Collectors.flatMapping(obs -> {
                List<TimeValuePair> pairs = new ArrayList<>();

                long start = obs.getStartTime();
                int sampleRate = obs.getSampleRate();
                String valueString = obs.getValues();

                // timestep in milliseconds
                double timestep = (1 / sampleRate) * 1000;

                String[] values = valueString.split("\\^");
                for (int i = 0; i < values.length; i++) {
                    long time = Double.valueOf(start + (i * timestep)).longValue();
                    double value = Double.parseDouble(values[i]);

                    pairs.add(new TimeValuePair(time, value));
                }

                return pairs.stream();
            }, Collectors.toList())));

        return timeValuePairs;
    }

}
