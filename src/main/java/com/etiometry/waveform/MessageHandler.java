package com.etiometry.waveform;

import java.util.List;

import org.dellroad.hl7.HL7Message;

import com.etiometry.waveform.model.Patient;
import com.etiometry.waveform.model.WaveformObservation;
import com.etiometry.waveform.model.WaveformStringObservation;

public class MessageHandler {
    private final PatientHandler patientHandler;
    private final WaveformReader waveformReader;
    private final WaveformPersister waveformPersister;

    public MessageHandler() {
        this.patientHandler = new PatientHandler();
        this.waveformReader = new WaveformReader();
        this.waveformPersister = new WaveformPersister();
    }

    public void handle(HL7Message msg, String persistType, int batchSize, String obsType) throws Exception {
        String mshTime = msg.get("MSH.7");
        String msgId = msg.get("MSH.10");
        String mrn = msg.get("PID.3.1");
        String lastName = msg.get("PID.5.1");
        String firstName = msg.get("PID.5.2");
        String bedId = msg.get("PV1.3");
        String obrTime = msg.get("OBR.7");

        // System.out.println(MessageFormat.format(
        // "mshTime: {0}, msgId: {1}, mrn: {2}, bedId: {3}, obrTime: {4}",
        // mshTime, msgId, mrn, bedId, obrTime));

        long t0 = System.currentTimeMillis();
        Patient patient = patientHandler.save(mrn, lastName, firstName);
        long t1 = System.currentTimeMillis();

        // System.out.println("*** total patient save time: " + (t1 - t0) + "ms");

        switch (obsType) {
        case "int":
            long t3 = System.currentTimeMillis();
            List<WaveformObservation> observations = waveformReader.read(patient, msg);
            long t4 = System.currentTimeMillis();

            // System.out.println("*** total waveform value read time: " + (t4 - t3) + "ms");

            switch (persistType) {
            case "batch":
                waveformPersister.persistByBatchInsert(observations, batchSize);
                break;

            case "copy":
                waveformPersister.persistByParallelCopy(observations);
                break;
            }

            long t5 = System.currentTimeMillis();

            // System.out.println("*** total waveform observation persist time: " + (t5 - t4) + "ms");
            break;

        case "string":
            long t6 = System.currentTimeMillis();
            List<WaveformStringObservation> stringObservations = waveformReader.readWaveformAsStringObservations(
                patient, msg);
            long t7 = System.currentTimeMillis();

            // System.out.println("*** total waveform value read time: " + (t7 - t6) + "ms");

            waveformPersister.persistStringObservation(stringObservations);

            long t8 = System.currentTimeMillis();

            // System.out.println("*** total waveform observation persist time: " + (t8 - t7) + "ms");
            break;
        }

    }

}
