DROP TABLE IF EXISTS "waveform_observation";
DROP TABLE IF EXISTS "waveform_string_observation";
DROP TABLE IF EXISTS "patient";
DROP TABLE IF EXISTS "waveform";

create table patient (
    id serial primary key,
    mrn varchar(64),
    lastname varchar(64),
    firstname varchar(64)
);
create index idx_patient on patient (mrn);

create table waveform_observation (
    id varchar(64),
    obstimestamp bigint not null,
    waveformid smallint,
    value numeric,
    fkpatient int,
    foreign key (fkpatient) references patient
);

select create_hypertable('waveform_observation', 'obstimestamp', chunk_time_interval => 604800000);

create table waveform_string_observation (
    id varchar(64),
    startTime bigint not null,
    endTime bigint not null,
    waveformid smallint,
    values text,
    sampleRate smallint,
    fkpatient int,
    foreign key (fkpatient) references patient
);

select create_hypertable('waveform_string_observation', 'starttime', chunk_time_interval => 604800000);
create index on (fkpatient, starttime);

create table waveform (
    id serial primary key,
    label varchar(64)
);

insert into waveform (label) values ('MDC_ECG_ELEC_POTL_I');
insert into waveform (label) values ('MDC_ECG_ELEC_POTL_II');
insert into waveform (label) values ('MDC_ECG_ELEC_POTL_V2');
insert into waveform (label) values ('MDC_ECG_ELEC_POTL_III');
insert into waveform (label) values ('MDC_ECG_ELEC_POTL_AVR');
insert into waveform (label) values ('MDC_ECG_ELEC_POTL_AVL');
insert into waveform (label) values ('MDC_ECG_ELEC_POTL_AVF');
insert into waveform (label) values ('MDC_IMPED_TTHOR');
